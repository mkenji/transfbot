package com.itau.transfernowbot.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.itau.transfernowbot.model.UsuarioModel;

public class UsuarioService {
	static SessionFactory sf = null;
	
	public static UsuarioModel buscarUsuarioByUserName(String userName) {
		UsuarioModel usuario = new UsuarioModel();
		sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Query query = session.createQuery("from UsuarioModel where username = :username ");
		query.setParameter("username", userName);
		
		List<UsuarioModel> list = query.list();
		if(!list.isEmpty()) {
			usuario = list.get(0);
		}

		return usuario;
	}
	
}
