package com.itau.transfernowbot;

public enum Command {

	HELP("/help", "Exibe a lista de comandos"), 
	START("/start", "Inicia usuario no sistema"), 
	EXTRATO("/extrato", "Exibe o extrato de transações do cliente"), 
	SALDO("/saldo", "Exibe o saldo atual do cliente"),
	TRANSFERIR("/transferir", "Transfere valores entre clientes (Exemplo: /transferir @nomeUsuario 100,00");
	
	private String comando;
	private String descricao;
	
	private Command(String comando, String descricao) {
		this.comando = comando;
		this.descricao = descricao;
	}

	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
