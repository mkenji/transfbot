package com.itau.transfernowbot.commands;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.telegram.telegrambots.api.objects.Update;

import com.itau.transfernowbot.model.ContaModel;
import com.itau.transfernowbot.model.TransferenciaModel;
import com.itau.transfernowbot.model.UsuarioModel;

public class TransfereCommand {
	SessionFactory sf = null;

	public StringBuilder execute(Update update, String usuarioDestino, Double valor) {
		StringBuilder helpMessageBuilder = new StringBuilder("<b>Transferência de valores</b>\n");
		helpMessageBuilder.append("Usuario origem: <b>@"+update.getMessage().getFrom().getUserName()+"</b>\n");
		helpMessageBuilder.append("Usuario destino: <b>"+usuarioDestino+"</b>\n");
		helpMessageBuilder.append("Valor a ser transferido: <b>"+valor+"</b>\n");

		//TODO: implementar logica para realizar as chamadas dos metodos
		UsuarioModel usuarioOrg = TransfereCommand.this.retornarUsuarioPeloUserName((update.getMessage().getFrom().getUserName()));
		UsuarioModel usuarioDest = TransfereCommand.this.retornarUsuarioPeloUserName(usuarioDestino);

		ContaModel contaOrg = TransfereCommand.this.retornarContaDeUsuarioPeloIdUsuario(usuarioOrg.getIdUsuario());
		ContaModel contaDest = TransfereCommand.this.retornarContaDeUsuarioPeloIdUsuario(usuarioDest.getIdUsuario());

		//executa transferencia
		helpMessageBuilder = TransfereCommand.this.realizarTransferencia(contaOrg, contaDest, usuarioOrg, usuarioDest, valor);

		return helpMessageBuilder;

	}

	//realiza transferencia
	public StringBuilder realizarTransferencia(ContaModel contaSolicitante, ContaModel contaDestinatario,UsuarioModel usuarioSolicitante,UsuarioModel usuarioDestinatario, double valor) {
		StringBuilder retornoTexto = new StringBuilder();
		if(TransfereCommand.this.usuariosSaoValidos(usuarioSolicitante, usuarioDestinatario)) {

			if(TransfereCommand.this.verificarSaldoUsuario(contaSolicitante, valor)) {

				TransfereCommand.this.debitarValorSolicitante(contaSolicitante, valor);
				TransfereCommand.this.creditarValorDestinatario(contaDestinatario, valor);
				
				sf = new Configuration().configure().buildSessionFactory();
				Session session = sf.openSession();
				Query query = null;
				session.beginTransaction();
				query = session.createQuery("UPDATE ContaModel set saldo = :saldoSolicitante WHERE idConta = :idContaSolicitante");
				query.setParameter("saldoSolicitante", contaSolicitante.getSaldo());
				query.setParameter("idContaSolicitante", contaSolicitante.getIdConta());
				query.executeUpdate();
				
				query = session.createQuery("UPDATE ContaModel set saldo = :saldoDestinatario WHERE idConta = :idContaDestinatario");
				query.setParameter("saldoDestinatario", contaDestinatario.getSaldo());
				query.setParameter("idContaDestinatario", contaDestinatario.getIdConta());
				query.executeUpdate();
				
				TransferenciaModel transferencia = new TransferenciaModel();
				transferencia.setIdUsrEnvio(contaSolicitante.getUsuario().getIdUsuario());
				transferencia.setIdUsrRecebimento(contaDestinatario.getUsuario().getIdUsuario());
				transferencia.setValor(valor);
				session.save(transferencia);
				
				session.getTransaction().commit();
				session.close();
				return retornoTexto.append("Transferência realizada com sucesso");

			} 
			else {
				return retornoTexto.append("Solicitante com saldo insuficiente");
			}
		}
		else {
			return retornoTexto.append("Usuário inválido");
		}

	}


	//retornar usuario baseado no userName recebido 
	public UsuarioModel retornarUsuarioPeloUserName(String userNameRecebido) {	
		
		UsuarioModel usuario = new UsuarioModel();
		
		sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Query query = session.createQuery("from UsuarioModel where username = :username ");
		query.setParameter("username", userNameRecebido);

		List<UsuarioModel> list = query.list();
		if(!list.isEmpty()) {
			usuario = list.get(0);
		}

		return usuario;
	}

	//retornar conta do usuario baseado no userName recebido 
	public ContaModel retornarContaDeUsuarioPeloIdUsuario(int idUsr) {	

		ContaModel conta = new ContaModel();
		
		sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Query query = session.createQuery("from ContaModel where idUsuario = :idUsuarioRecebido ");
		query.setParameter("idUsuarioRecebido", idUsr);

		List<ContaModel> list = query.list();
		if(!list.isEmpty()) {
			conta = list.get(0);
		}

		return conta ;
	}


	//verificar se usuarios sao validos
	public boolean usuariosSaoValidos(UsuarioModel solicitante, UsuarioModel destinatario) {	
		if(solicitante.getNome().isEmpty() || destinatario.getNome() == null) {
			return false;
		}
		
		return true;
	}

	//verificar se o solicitante tem saldo
	public boolean verificarSaldoUsuario(ContaModel contaSolicitante,double valorAdebitar) {

		if(contaSolicitante.getSaldo()>= valorAdebitar) {
			return true;
		}
		else 
			return false;
	}

	//debitar saldo do solicitante
	public void debitarValorSolicitante(ContaModel solicitante, double valor) {

		solicitante.setSaldo(solicitante.getSaldo() - valor);

	}

	//creditar saldo do destinatario
	public void creditarValorDestinatario(ContaModel destinatario, double valor) {

		destinatario.setSaldo(destinatario.getSaldo() + valor);
	}


}
