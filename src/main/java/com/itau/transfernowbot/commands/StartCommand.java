package com.itau.transfernowbot.commands;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.telegram.telegrambots.api.objects.Update;

import com.itau.transfernowbot.Command;
import com.itau.transfernowbot.model.ContaModel;
import com.itau.transfernowbot.model.UsuarioModel;

public class StartCommand {
	SessionFactory sf = null;
	Query query = null;
	public StringBuilder execute(Update update) {
		StringBuilder startMessage = new StringBuilder();
		sf = new Configuration().configure().buildSessionFactory();
		
		Session session = sf.openSession();
		session.beginTransaction();
		
		query = session.createQuery("from UsuarioModel where username = :username ");
		query.setParameter("username", update.getMessage().getFrom().getUserName());
		List<UsuarioModel> list = query.list();
		
		startMessage.append("<b>BEM VINDO "+update.getMessage().getFrom().getFirstName()+"!</b>\n");
		
		if(update.getMessage().getFrom().getUserName() == null) {
			startMessage.append("Você não possui um usuário valido.\n");
			startMessage.append("Acesse o menu Configurações -> Username para criar seu usuário no Telegram.\n");
		}
		
		if(list.isEmpty()) {
			UsuarioModel usuario = new UsuarioModel();
			usuario.setNome(update.getMessage().getFrom().getFirstName()+" "+update.getMessage().getFrom().getLastName());
			usuario.setUsername(update.getMessage().getFrom().getUserName());
			usuario.setChatId(update.getMessage().getChatId().toString());
			Serializable idNewUsuario = session.save(usuario);
			
			ContaModel conta = new ContaModel();
			conta.setUsuario(usuario);
			conta.setSaldo(100.00);
			session.save(conta);
			
			session.getTransaction().commit();
			startMessage.append("<i>Usuário criado no sistema</i>\n\n");
		}
		
		startMessage.append("Esse bot contém os seguintes comandos:\n");
		for(Command comandos : Command.values()) {
			startMessage.append(comandos.getComando()+" - "+comandos.getDescricao()).append("\n");
        }
		
		return startMessage;
		
	}
}
