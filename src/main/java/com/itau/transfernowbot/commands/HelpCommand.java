package com.itau.transfernowbot.commands;

import org.telegram.telegrambots.api.objects.Update;

import com.itau.transfernowbot.Command;

public class HelpCommand {

	public StringBuilder execute(Update update) {
		StringBuilder helpMessageBuilder = new StringBuilder("<b>Help</b>\n");
        helpMessageBuilder.append("<i>Ações registradas para esse bot:</i>\n\n");

        for(Command comandos : Command.values()) {
        	helpMessageBuilder.append(comandos.getComando()+" - "+comandos.getDescricao()).append("\n");
        }
        
        return helpMessageBuilder;
	}

	
}
