package com.itau.transfernowbot.commands;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.telegram.telegrambots.api.objects.User;

import com.itau.transfernowbot.model.ContaModel;
import com.itau.transfernowbot.model.TransferenciaModel;
import com.itau.transfernowbot.model.UsuarioModel;


public class ExtratoCommand {
	SessionFactory sf = null;

	public StringBuilder execute(User user) {
		sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();

		//Consulta o ID pelo usuario do Telegram
		Query queryId = session.createQuery("from UsuarioModel where username = :nick");
		queryId.setParameter("nick", user.getUserName());
		List<UsuarioModel> resultId = queryId.list();

		//Busca as transferencias do usuario
		Query queryTransf = session.createQuery("from TransferenciaModel where idUsrEnvio = :id or idUsrRecebimento = :id");				
		queryTransf.setParameter("id",resultId.get(0).getIdUsuario());
		List<TransferenciaModel>  resultExtrato = queryTransf.list();

		//Busca o saldo do usuario
		Query querySaldo = session.createQuery("from ContaModel where idUsuario = :id");				
		querySaldo.setParameter("id", resultId.get(0).getIdUsuario());	
		List<ContaModel> resultSaldo = querySaldo.list();

		//Formata o retorno para o BOT
		StringBuilder extratoMessageBuilder = new StringBuilder("<b>Seu Extrato</b>\n");
		if(resultExtrato.isEmpty()) {
			extratoMessageBuilder.append("Você não possuí transferências");
			return(extratoMessageBuilder);
		} else {
			for(TransferenciaModel transf: resultExtrato){ 
				if(resultId.get(0).getIdUsuario() == transf.getIdUsrEnvio()) {
					extratoMessageBuilder.append("Enviado: " + transf.getDataHora()+" | (-)"+transf.getValor()).append("\n");	
				} else {
					extratoMessageBuilder.append("Recebido: " + transf.getDataHora()+" | (+)"+transf.getValor()).append("\n");
				}	
			}
			extratoMessageBuilder.append("Saldo Atualizado: " + resultSaldo.get(0).getSaldo());
			return(extratoMessageBuilder);
		}	
	}
}

