package com.itau.transfernowbot.commands;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.telegram.telegrambots.api.objects.User;
import com.itau.transfernowbot.model.ContaModel;
import com.itau.transfernowbot.model.UsuarioModel;

//TODO: Implementar metodo para exibir o saldo do usuario
public class SaldoCommand {
	Query query = null;
	public StringBuilder execute(User user) {
		StringBuilder startMessage = new StringBuilder();
		SessionFactory sf = null;


		sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();

		query = session.createQuery("from UsuarioModel where username = :username ");
		query.setParameter("username", user.getUserName());
		List<UsuarioModel> listUsuario = query.list();


		query = session.createQuery("from ContaModel where idUsuario = :idUsuario ");
		query.setParameter("idUsuario", listUsuario.get(0).getIdUsuario());
		List<ContaModel> listConta = query.list();

		startMessage.append("Seu saldo é:"+ listConta.get(0).getSaldo());
		return startMessage;

	}
}




