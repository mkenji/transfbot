package com.itau.transfernowbot;


import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import com.itau.transfernowbot.commands.ExtratoCommand;
import com.itau.transfernowbot.main.MainBot;

public class App 
{
    public static void main(String[] args)
    {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotApi = new TelegramBotsApi();
        MainBot bot = new MainBot();
        
        try {
        	telegramBotApi.registerBot(bot);
        }
        catch(TelegramApiRequestException e) {
        	e.printStackTrace();
        }
    }
}