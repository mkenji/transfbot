package com.itau.transfernowbot.main;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import com.itau.transfernowbot.commands.ExtratoCommand;
import com.itau.transfernowbot.commands.HelpCommand;
import com.itau.transfernowbot.commands.SaldoCommand;
import com.itau.transfernowbot.commands.StartCommand;
import com.itau.transfernowbot.commands.TransfereCommand;
import com.itau.transfernowbot.model.UsuarioModel;
import com.itau.transfernowbot.service.UsuarioService;


public class MainBot extends TelegramLongPollingBot {

	public String getBotUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	public void onUpdateReceived(Update update) {
		
		String command = update.getMessage().getText();
		SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId());
		sendMessage.enableHtml(true);
		StringBuilder mensagem = new StringBuilder();
		boolean sendToDest = false;
		String valorTransferido = ""; 
		String usuarioDest = "";
		
		if(command.equals("/start")){
			StartCommand start = new StartCommand();
			mensagem = start.execute(update);
		} else if(command.equals("/extrato")){
			ExtratoCommand extrato = new ExtratoCommand();
			mensagem = extrato.execute(update.getMessage().getFrom());
		} else if(command.equals("/saldo")){
			SaldoCommand saldo = new SaldoCommand();
			mensagem = saldo.execute(update.getMessage().getFrom());
		} else if(command.equals("/help")){
			HelpCommand help = new HelpCommand();
			mensagem = help.execute(update);
		} else if(command.contains("/transferir")) {
			String arrayComandos[] = command.split(" ");
			//verifico se enviou a mensagem com todos os parametros
			if(arrayComandos.length == 3) {
				TransfereCommand transfere = new TransfereCommand();
				valorTransferido = arrayComandos[2];
				String valor = arrayComandos[2].replaceAll(",", ".");
				usuarioDest = arrayComandos[1].replaceAll("@", "");
				mensagem = transfere.execute(update, usuarioDest, Double.parseDouble(valor));
				sendToDest = true;
			} else {
				mensagem.append("ERRO! Favor enviar o comando /transferir com os parametros @nome_do_usuario e valor. Exemplo: /transferir @usuario 100,00");
			}
		} else {
			mensagem.append("<b>Comando enviado inválido</b> \n");
			HelpCommand help = new HelpCommand();
			mensagem.append(help.execute(update));
		}
		
		sendMessage.setText(mensagem.toString());
				
		try {
			//Enviar mensagem de retorno para usuario
			sendMessage(sendMessage);
			
			if(sendToDest) {
				mensagem.setLength(0);
				String userNameFull = update.getMessage().getFrom().getFirstName()+" "+update.getMessage().getFrom().getLastName()+" ("+update.getMessage().getFrom().getUserName()+")";
				mensagem.append("Você recebeu um crédito de "+valorTransferido+" do usuario "+userNameFull+". Verifique seu saldo.");
				UsuarioModel usuario = UsuarioService.buscarUsuarioByUserName(usuarioDest);
				SendMessage sendMessageDest = new SendMessage().setChatId(usuario.getChatId());
				sendMessageDest.enableHtml(true);
				sendMessageDest.setText(mensagem.toString());
				sendMessage(sendMessageDest);
			}
		}
		catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}
		
	@Override
	public String getBotToken() {
		// TODO Auto-generated method stub
//		return "542313160:AAG_Fl-4ptPpksDgk7buSv3HOVWizHVePEc";
		//return "487409867:AAHKSlcDoh1qmIIIREoNnQNTsWrAQ4xQkyQ";
		return System.getenv("BOT_TOKEN");
	}

	
	
}
