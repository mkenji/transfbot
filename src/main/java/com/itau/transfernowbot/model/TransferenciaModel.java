package com.itau.transfernowbot.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="Transferencia")
public class TransferenciaModel {	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idTransferencia;
	
	private int idUsrEnvio;
	
	private int idUsrRecebimento;
	@CreationTimestamp
	private Timestamp dataHora;
	private double valor;
	
	public Timestamp getDataHora() {
		return dataHora;
	}
	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}
	public int getIdTransferencia() {
		return idTransferencia;
	}
	public void setIdTransferencia(int idTransferencia) {
		this.idTransferencia = idTransferencia;
	}
	public int getIdUsrEnvio() {
		return idUsrEnvio;
	}
	public void setIdUsrEnvio(int idUsrEnvio) {
		this.idUsrEnvio = idUsrEnvio;
	}
	public int getIdUsrRecebimento() {
		return idUsrRecebimento;
	}
	public void setIdUsrRecebimento(int idUsrRecebimento) {
		this.idUsrRecebimento = idUsrRecebimento;
	}

	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	

}
