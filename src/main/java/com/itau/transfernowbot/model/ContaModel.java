package com.itau.transfernowbot.model;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;


@Entity
@Table(name="Conta")
public class ContaModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idConta;
	
	@OneToOne(targetEntity=UsuarioModel.class)
	@JoinColumn(name="idUsuario")
	private UsuarioModel usuario;
	
	private double saldo;
	@CreationTimestamp
	private Timestamp dataAbertura;

	
	
	public int getIdConta() {
		return idConta;
	}
	public void setIdConta(int idConta) {
		this.idConta = idConta;
	}
	public UsuarioModel getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioModel usuario) {
		this.usuario = usuario;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public Timestamp getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Timestamp dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
}
